// Copyright © 2018 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"io"
	"net/http"
	"os"
)

// downloadAttachmentCmd represents the downloadAttachment command
var downloadAttachmentCmd = &cobra.Command{
	Use:   "download-attachment",
	Short: "command to download jira attachment",
	Run:   downloadAttachment,
}

// downloadAttachment will download jira attachment in specific issue,
// user can provide the output path for the downloaded path, if not
// provided then it will saved into current work dir and will use
// default name in attachment metadata (that's why issue ID needed)
func downloadAttachment(cmd *cobra.Command, args []string) {
	jiraClient := authJira()
	downloaded, err := jiraClient.Issue.DownloadAttachment(attachmentId)

	if err != nil {
		fmt.Println(err)
	}

	if downloaded.Response.StatusCode == http.StatusOK {
		var out *os.File

		if outputPath == "" {
			attachmentMeta := getAttachmentMetadata(issueId, attachmentId)
			out, err = os.Create(attachmentMeta.Filename)
		} else {
			out, err = os.Create(outputPath)
		}

		if err != nil {
			fmt.Println(err.Error())
		}

		defer out.Close()
		_, err := io.Copy(out, downloaded.Response.Body)
		if err != nil {
			fmt.Println(err.Error())
		}
	}
}

func init() {
	jiraCmd.AddCommand(downloadAttachmentCmd)

	downloadAttachmentCmd.Flags().StringVarP(
		&issueId, "issue-id", "i", "", "jira issue id")
	downloadAttachmentCmd.Flags().StringVarP(
		&attachmentId, "attachment-id", "a", "", "attachment ID that will be downloaded")
	downloadAttachmentCmd.Flags().StringVarP(
		&outputPath, "output", "o", "", "output path for downloaded attachment")
	downloadAttachmentCmd.MarkFlagRequired("issue-id")
	downloadAttachmentCmd.MarkFlagRequired("attachment-id")
}
