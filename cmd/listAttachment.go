// Copyright © 2018 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"fmt"
	"github.com/olekukonko/tablewriter"
	"os"

	"github.com/spf13/cobra"
)

// listAttachmentCmd represents the listAttachment command
var listAttachmentCmd = &cobra.Command{
	Use:   "list-attachments",
	Short: "command to list attachment of specific JIRA issue",
	Run:   listAttachment,
}

// listAttachment used to list attachments of specific jira issue,
// it will gives list with ID, Filename, and Created Date of the attachments
func listAttachment(cmd *cobra.Command, args []string) {
	var attachments [][]string

	jiraClient := authJira()
	issue, _, _ := jiraClient.Issue.Get(issueId, nil)

	if len(issue.Fields.Attachments) > 0 {
		table := tablewriter.NewWriter(os.Stdout)
		table.SetHeader([]string{"ID", "Filename", "Created Date"})

		for i := 0; i < len(issue.Fields.Attachments); i++ {
			tmp := issue.Fields.Attachments[i]
			attachments = append(attachments, []string{
				tmp.ID, tmp.Filename, tmp.Created,
			})
		}

		for _, v := range attachments {
			table.Append(v)
		}
		table.Render()
	} else {
		fmt.Println("No attachments available for this issue")
	}
}

func init() {
	jiraCmd.AddCommand(listAttachmentCmd)

	listAttachmentCmd.Flags().StringVarP(&issueId, "issue-id", "i", "", "JIRA issue ID")
	listAttachmentCmd.MarkFlagRequired("issue-id")
}
