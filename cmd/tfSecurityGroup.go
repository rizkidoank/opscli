// Copyright © 2018 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/service/ec2"
	"github.com/spf13/cobra"
	"html/template"
	"log"
	"os"
	"strings"
)

const securityGroupResource = `
resource "aws_security_group" "{{.groupName}}" {
    name        = "{{.groupName}}"
    description = "{{.groupName}} security group"
    vpc_id      = "${local.vpc_id}"

    tags = {
        Name          = "{{.groupName}}"
        Service       = "{{.serviceName}}"
        ProductDomain = "{{.productDomain}}"
        ManagedBy     = "Terraform"
    }
}
`

const securityGroupRuleResource = `
{{- if eq .sourceType "sgname"}}
resource "aws_security_group_rule" "{{.destination}}-from-{{.source}}" {
    security_group_id           = "${aws_security_group.{{.destination}}.id}"
{{- if eq .sourceService .destinationService}}
    source_security_group_id    = "${aws_security_group.{{.source}}.id}"
{{else}}
    source_security_group_id    = "{{.sourceId}}"
{{end}}
    from_port                   = {{.fromPort}}
    to_port                     = {{.toPort}}
    protocol                    = "{{.protocol}}"
    type                        = "ingress"
    description                 = "ingress from {{.source}} to {{.destination}}"
}
{{else if eq .sourceType "iprange"}}
resource "aws_security_group_rule" "{{.destination}}-from-{{.sourceIdentifier}}" {
    security_group_id           = "${aws_security_group.{{.destination}}.id}"
    cidr_blocks                 = ["{{.source}}"]

    from_port                   = {{.fromPort}}
    to_port                     = {{.toPort}}
    protocol                    = "{{.protocol}}"
    type                        = "ingress"
}
{{end -}}
`

var (
	groupName string
)

// tfSecurityGroupCmd represents the tfSecurityGroup command
var tfSecurityGroupCmd = &cobra.Command{
	Use:   "tf-security-group",
	Short: "command to generate security group terraform config",
	Long: `
This command used to generate terraform config for security group resource. 
Acceptable source are local JSON file.`,
	Run: generateTfSecurityGroup,
}

// awsGetGroupId will get group ID filtered by group name, if not exists return "None"
func awsGetGroupId(groupName string, vpcId string) string {
	svc := ec2.New(sess)
	result, err := svc.DescribeSecurityGroups(
		&ec2.DescribeSecurityGroupsInput{
			Filters: []*ec2.Filter{
				{
					Name: aws.String("group-name"),
					Values: []*string{
						aws.String(groupName),
					},
				},
				{
					Name: aws.String("vpc-id"),
					Values: []*string{
						aws.String(vpcId),
					},
				},
			},
		})

	if err != nil || len(result.SecurityGroups) < 1 {
		if _, ok := err.(awserr.Error); ok {
			fmt.Println(err.Error())
		}
		return "None"
	}
	return aws.StringValue(result.SecurityGroups[0].GroupId)
}

// awsGetGroupName will get group name filtered by group ID, if not exists return "None"
func awsGetGroupName(group_id string) string {
	svc := ec2.New(sess)
	result, err := svc.DescribeSecurityGroups(
		&ec2.DescribeSecurityGroupsInput{
			GroupIds: []*string{
				aws.String(group_id),
			},
		},
	)

	if err != nil {
		if _, ok := err.(awserr.Error); ok {
			fmt.Println(err.Error())
		}
		return ""
	}

	return aws.StringValue(result.SecurityGroups[0].GroupName)
}

// gatherConnectivityDetails will get required details so its easier
// to parse. This is used as a helper so it can be used to generate code
// for terraform config.
func gatherConnectivityDetails(res Resources) Resources {
	for i := 0; i < len(res.Resources.Connectivity); i++ {
		switch res.Resources.Connectivity[i].DestinationType {
		case "sgname":
			res.Resources.Connectivity[i].DestinationId = awsGetGroupId(res.Resources.Connectivity[i].Destination, getVPCId(vpcName))
		case "sgid":
			res.Resources.Connectivity[i].DestinationId = res.Resources.Connectivity[i].Destination
			res.Resources.Connectivity[i].Destination = awsGetGroupName(res.Resources.Connectivity[i].DestinationId)
			if res.Resources.Connectivity[i].Destination != "None" {
				res.Resources.Connectivity[i].DestinationType = "sgname"
			}
		}
		switch res.Resources.Connectivity[i].SourceType {
		case "sgname":
			res.Resources.Connectivity[i].SourceId = awsGetGroupId(res.Resources.Connectivity[i].Source, getVPCId(vpcName))
		case "sgid":
			res.Resources.Connectivity[i].SourceId = res.Resources.Connectivity[i].Source
			res.Resources.Connectivity[i].Source = awsGetGroupName(res.Resources.Connectivity[i].SourceId)
			if res.Resources.Connectivity[i].Source != "None" {
				res.Resources.Connectivity[i].SourceType = "sgname"
			}
		}
	}

	return res
}

// stringInSlice is a function that will check whether specific string exists
// in a slice. Basically like `for string in list` in python
func stringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}

// generateTfSecurityGroup will generate terraform config for security group.
// it will parse resources json and then get necessary details. After that,
// it will check whether the source and destination are in same service.
// If its in same service it will use reference `${aws_security_group.<group-name>.id}`.
// Its also will create `aws_security_group` resource if not exists.
func generateTfSecurityGroup(cmd *cobra.Command, args []string) {
	resources := extractResources(sourcePath)
	resources = gatherConnectivityDetails(resources)
	var nonExistsSg []string

	// check any non-exists group in same services
	for i := 0; i < len(resources.Resources.Connectivity); i++ {
		rule := resources.Resources.Connectivity[i]
		if rule.Destination == groupName {
			if rule.SourceType != "sgname" ||
				(strings.Split(rule.Source, "-")[0] == strings.Split(groupName, "-")[0]) {
				if rule.SourceId == "None" && !stringInSlice(rule.Source, nonExistsSg) {
					nonExistsSg = append(nonExistsSg, rule.Source)
				}
				if rule.DestinationId == "None" && !stringInSlice(rule.Destination, nonExistsSg) {
					nonExistsSg = append(nonExistsSg, rule.Destination)
				}
			}
		}
	}

	// create aws_security_group resource if not exist
	for i := 0; i < len(nonExistsSg); i++ {
		if awsGetGroupId(nonExistsSg[i], getVPCId(vpcName)) == "None" {
			tmpl, err := template.New("sg_resource").Parse(securityGroupResource)
			if err != nil {
				log.Fatal(err)
			}

			varmap := map[string]interface{}{
				"groupName":     nonExistsSg[i],
				"serviceName":   strings.Split(nonExistsSg[i], "-")[0],
				"productDomain": resources.ProductDomain,
			}
			tmpl.Execute(os.Stdout, varmap)
		}
	}

	for i := 0; i < len(resources.Resources.Connectivity); i++ {
		rule := resources.Resources.Connectivity[i]
		if rule.Destination == groupName {
			switch rule.SourceType {
			case "sgname":
				tmpl, err := template.New("sg_resource").Parse(securityGroupRuleResource)
				if err != nil {
					log.Fatal(err)
				}

				varmap := map[string]interface{}{
					"sourceType":         rule.SourceType,
					"source":             rule.Source,
					"sourceId":           rule.SourceId,
					"sourceService":      strings.Split(rule.Source, "-")[0],
					"destination":        rule.Destination,
					"destinationId":      rule.DestinationId,
					"destinationService": strings.Split(rule.Destination, "-")[0],
					"fromPort":           rule.FromPort,
					"toPort":             rule.ToPort,
					"protocol":           rule.Protocol,
				}
				tmpl.Execute(os.Stdout, varmap)

			case "iprange":
				tmpl, err := template.New("sg_resource").Parse(securityGroupRuleResource)
				if err != nil {
					log.Fatal(err)
				}

				varmap := map[string]interface{}{
					"sourceType":       rule.SourceType,
					"source":           rule.Source,
					"sourceIdentifier": strings.Replace(strings.Split(rule.Source, "/")[0], ".", "-", -1),
					"destination":      rule.Destination,
					"destinationId":    rule.DestinationId,
					"fromPort":         rule.FromPort,
					"toPort":           rule.ToPort,
					"protocol":         rule.Protocol,
				}
				tmpl.Execute(os.Stdout, varmap)
			}
		}
	}
}

func init() {
	codegenCmd.AddCommand(tfSecurityGroupCmd)

	tfSecurityGroupCmd.Flags().StringVarP(&sourcePath, "file", "i", "", "source file path")
	tfSecurityGroupCmd.Flags().StringVarP(&groupName, "group-name", "n", "", "group name config")
	tfSecurityGroupCmd.Flags().StringVarP(&vpcName, "vpc-name", "p", "", "vpc tag name")

	tfSecurityGroupCmd.MarkFlagRequired("file")
	tfSecurityGroupCmd.MarkFlagRequired("group-name")
	tfSecurityGroupCmd.MarkFlagRequired("vpc-name")
}
