// Copyright © 2018 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"github.com/andygrunwald/go-jira"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var (
	issueId      string
	attachmentId string
	outputPath   string
)

// getAttachmentMetadata will get attachment metadata with specific ID in JIRA.
// To get the metadata, issueId is needed, because it will get the details from
// issue fields.
func getAttachmentMetadata(issueId string, attachmentId string) *jira.Attachment {
	jiraClient := authJira()
	issue, _, _ := jiraClient.Issue.Get(issueId, nil)
	for i := 0; i < len(issue.Fields.Attachments); i++ {
		tmpAttachment := issue.Fields.Attachments[i]
		if tmpAttachment.ID == attachmentId {
			return tmpAttachment
		}
	}
	return nil
}

// authJira will setup basic authentication credentials and then return new jira client
func authJira() *jira.Client {
	authTransport := jira.BasicAuthTransport{
		Username: viper.GetString("jira.username"),
		Password: viper.GetString("jira.password"),
	}
	jiraClient, _ := jira.NewClient(authTransport.Client(), viper.GetString("jira.url"))

	return jiraClient
}

// jiraCmd represents the jira command
var jiraCmd = &cobra.Command{
	Use:   "jira",
	Short: "set of commands to work with jira",
	Run: func(cmd *cobra.Command, args []string) {
		cmd.Help()
	},
}

func init() {
	rootCmd.AddCommand(jiraCmd)
}
