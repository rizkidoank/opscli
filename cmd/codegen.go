// Copyright © 2018 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"encoding/json"
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/ec2"
	"github.com/mitchellh/go-homedir"
	"github.com/spf13/cobra"
	"io/ioutil"
	"os"
)

// Struct for security group resource
type Resources struct {
	ProductDomain string `json:"product_domain"`
	Resources     struct {
		Connectivity []struct {
			SourceType      string `json:"source_type"`
			Source          string `json:"source"`
			SourceId        string `json:"source_id"`
			DestinationType string `json:"destination_type"`
			Destination     string `json:"destination"`
			DestinationId   string `json:"destination_id"`
			FromPort        int    `json:"from_port"`
			ToPort          int    `json:"to_port"`
			Protocol        string `json:"protocol"`
		} `json:"connectivity"`
		Asg []struct {
			ServiceName            string   `json:"service_name"`
			ClusterRole            string   `json:"cluster_role"`
			Application            string   `json:"application"`
			Description            string   `json:"description"`
			SecurityGroups         []string `json:"security_groups"`
			InstanceType           string   `json:"instance_type"`
			EbsOptimized           bool     `json:"ebs_optimized"`
			Monitoring             bool     `json:"monitoring"`
			LbTargetGroupArns      string   `json:"lb_target_group_arns"`
			HealthCheckType        string   `json:"health_check_type"`
			HealthCheckGracePeriod int      `json:"health_check_grace_period"`
			DefaultCooldown        int      `json:"default_cooldown"`
		} `json:"asg"`
		Alb []struct {
			ServiceName                                  string   `json:"service_name"`
			Internal                                     bool     `json:"is_internal"`
			Description                                  string   `json:"description"`
			SecurityGroups                               []string `json:"security_groups"`
			IdleTimeout                                  int      `json:"idle_timeout"`
			EnableDeletionProtection                     bool     `json:"enable_deletion_protection"`
			MainTargetGroupClusterRole                   string   `json:"main_target_group_cluster_role"`
			MainTargetGroupDescription                   string   `json:"main_target_group_description"`
			MainTargetGroupProtocol                      string   `json:"main_target_group_protocol"`
			MainTargetGroupPort                          int      `json:"main_target_group_port"`
			MainTargetGroupStickinessCookieDuration      int      `json:"main_target_group_stickiness_cookie_duration"`
			MainTargetGroupHealthCheckProtocol           int      `json:"main_target_group_health_check_protocol"`
			MainTargetGroupHealthCheckPath               string   `json:"main_target_group_health_check_path"`
			MainTargetGroupHealthCheckHealthyThreshold   int      `json:"main_target_group_health_check_healthy_threshold"`
			MainTargetGroupHealthCheckUnhealthyThreshold int
			MainTargetGroupHealthCheckTimeout            int `json:"main_target_group_health_check_timeout"`
			MainTargetGroupHealthCheckInterval           int `json:"main_target_group_health_check_interval"`
			MainTargetGroupHealthCheckMatcher            int `json:"main_target_group_health_check_matcher"`
			AdditionalTargetGroups                       []struct {
				ClusterRole                   string `json:"cluster_role"`
				Description                   string `json:"description"`
				Protocol                      string `json:"protocol"`
				Port                          int    `json:"port"`
				StickinessCookieDuration      int    `json:"stickiness_cookie_duration"`
				HealthCheckPort               int    `json:"health_check_port"`
				HealthCheckPath               string `json:"health_check_path"`
				HealthCheckHealthyThreshold   int    `json:"health_check_healthy_threshold"`
				HealthCheckUnhealthyThreshold int    `json:"health_check_unhealthy_threshold"`
				HealthCheckTimeout            int    `json:"health_check_timeout"`
				HealthCheckInterval           int    `json:"health_check_interval"`
				HealthCheckMatcher            int    `json:"health_check_matcher"`
				ListenerPriority              int    `json:"listener_priority"`
				ListenerConditionPath         string `json:"listener_condition_path"`
			} `json:"additional_target_groups"`
		} `json:"alb"`
	} `json:"resources"`
}

var (
	sourcePath string
	vpcName    string
	resources  Resources
	sess       = session.Must(session.NewSessionWithOptions(session.Options{
		SharedConfigState: session.SharedConfigEnable,
	}))
)

// extractResources will parse json of resources definition.
func extractResources(sourcePath string) Resources {
	var tmpRes Resources

	filePath, err := homedir.Expand(sourcePath)
	if err != nil {
		fmt.Println(err)
	}

	jsonFile, err := os.Open(filePath)
	if err != nil {
		fmt.Println(err)
	}

	defer jsonFile.Close()

	byteValue, _ := ioutil.ReadAll(jsonFile)
	json.Unmarshal(byteValue, &tmpRes)

	return tmpRes
}

func getVPCId(name string) string {
	svc := ec2.New(sess)
	result, err := svc.DescribeVpcs(
		&ec2.DescribeVpcsInput{
			Filters: []*ec2.Filter{
				{
					Name: aws.String("tag:Name"),
					Values: []*string{
						aws.String(name),
					},
				},
			},
		})
	if err != nil || len(result.Vpcs) < 1 {
		if _, ok := err.(awserr.Error); ok {
			fmt.Println(err.Error())
		}
		return "None"
	}
	return aws.StringValue(result.Vpcs[0].VpcId)
}

// codegenCmd represents the codegen command
var codegenCmd = &cobra.Command{
	Use:   "codegen",
	Short: "set of commands for code generator related",
	Run: func(cmd *cobra.Command, args []string) {
		cmd.Help()
	},
}

func init() {
	rootCmd.AddCommand(codegenCmd)
}
