// Copyright © 2018 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"github.com/spf13/cobra"
	"html/template"
	"log"
	"os"
	"strings"
)

const autoScalingGroupResource = `
locals {
  product_domain = "{{.productDomain}}"

  service_name = "{{.serviceName}}"
  application  = "{{.application}}"

  {{.clusterRole}}_description = "{{.description}}"
  {{.serviceName}}_{{.clusterRole}}_instance_type = "{{.instanceType}}"
  {{.serviceName}}_{{.clusterRole}}_health_check_type = "{{.healthCheckType}}"
  {{.serviceName}}_{{.clusterRole}}_health_check_grace_period = "{{.healthCheckGracePeriod}}"
  {{.serviceName}}_{{.clusterRole}}_default_cooldown = "{{.defaultCooldown}}"

  environment  = ""
  asg_subnets = [""]
  ami_id = ""
}

module "{{.serviceName}}-{{.clusterRole}}" {
  source         = "git::https://github.com/traveloka/terraform-aws-autoscaling.git?ref=v0.1.6-rolling"
  service_name   = "${local.service_name}"
  cluster_role   = "{{.clusterRole}}"
  environment    = "${local.environment}"
  application    = "${local.application}"
  product_domain = "${local.product_domain}"
  description    = "${local.{{.clusterRole}}_description}"

  asg_health_check_type         = "${local.{{.serviceName}}_{{.clusterRole}}_health_check_type}"
  asg_health_check_grace_period = "${local.{{.serviceName}}_{{.clusterRole}}_health_check_grace_period}"
  asg_default_cooldown          = "${local.{{.serviceName}}_{{.clusterRole}}_default_cooldown}"
  asg_min_capacity              = 0
  asg_vpc_zone_identifier       = "${local.asg_subnets}"
  asg_wait_for_capacity_timeout = "4m"


  lc_instance_profile = "${module.{{.serviceName}}-{{.clusterRole}}.instance_profile_arn}"
  lc_instance_type    = "${local.instance_type}"
  lc_ami_id           = "${local.ami_id}"
  lc_key_name         = "ec2_key"
  lc_ebs_optimized    = "{{.ebsOptimized}}"
  lc_monitoring       = "{{.monitoring}}"
  lc_user_data        = " "

  lc_security_groups = [
  {{- range $i, $sg := .securityGroups}}
    "{{$sg}}",
  {{- end}}
  ]
}
`

// tfAutoscalingGroupCmd represents the tfAutoscalingGroup command
var tfAutoscalingGroupCmd = &cobra.Command{
	Use:   "tf-autoscaling-group",
	Short: "command to generate autoscaling group terraform config",
	Run:   tfAutoScalingGroup,
}

func tfAutoScalingGroup(cmd *cobra.Command, args []string) {
	resources := extractResources(sourcePath)
	for i := 0; i < len(resources.Resources.Asg); i++ {
		tmpAsg := resources.Resources.Asg[i]
		tmpl, err := template.New("asg_resource").Parse(autoScalingGroupResource)
		if err != nil {
			log.Fatal(err)
		}

		var groupList []string
		for j := 0; j < len(tmpAsg.SecurityGroups); j++ {
			groupSplit := strings.Split(tmpAsg.SecurityGroups[j], "-")
			if groupSplit[0] == tmpAsg.ServiceName {
				tmp := "${aws_security_group." + tmpAsg.SecurityGroups[j] + ".id}"
				groupList = append(groupList, tmp)
			} else {
				tmp := awsGetGroupId(tmpAsg.SecurityGroups[j], getVPCId(vpcName))
				groupList = append(groupList, tmp)
			}
		}

		varmap := map[string]interface{}{
			"productDomain":          resources.ProductDomain,
			"serviceName":            tmpAsg.ServiceName,
			"application":            tmpAsg.Application,
			"description":            tmpAsg.Description,
			"instanceType":           tmpAsg.InstanceType,
			"healthCheckType":        tmpAsg.HealthCheckType,
			"healthCheckGracePeriod": tmpAsg.HealthCheckGracePeriod,
			"defaultCooldown":        tmpAsg.DefaultCooldown,
			"clusterRole":            tmpAsg.ClusterRole,
			"ebsOptimized":           tmpAsg.EbsOptimized,
			"monitoring":             tmpAsg.Monitoring,
			"securityGroups":         groupList,
		}
		tmpl.Execute(os.Stdout, varmap)
	}
}

func init() {
	codegenCmd.AddCommand(tfAutoscalingGroupCmd)

	tfAutoscalingGroupCmd.Flags().StringVarP(&sourcePath, "file", "i", "", "source file path")
	tfAutoscalingGroupCmd.MarkFlagRequired("file")

	tfAutoscalingGroupCmd.Flags().StringVarP(&vpcName, "vpc-name", "p", "", "vpc tag name")
	tfAutoscalingGroupCmd.MarkFlagRequired("vpc-name")
}
