// Copyright © 2018 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"github.com/spf13/cobra"
	"html/template"
	"log"
	"os"
	"strings"
)

const intAppLoadBalancerResource = `
locals {
  service_name = "{{.serviceName}}"
  domain_name  = "main.tvlk.cloud"
  vpc_id       = "{{.vpcId}}"

  {{.serviceName}}_lbint_prefix = "{{.serviceName}}-lbint"
  {{.serviceName}}_tgint_prefix = "{{.serviceName}}-{{.mainTargetGroupClusterRole}}"

  {{.serviceName}}_lbint_cert_arn                    = ""
  {{.serviceName}}_lbint_log_bucket}                 = ""
  {{.serviceName}}_tgint_stickiness_duration         = "{{.mainTargetGroupStickinessCookieDuration}}" 
  {{.mainTargetGroupClusterRole}}_service_port       = "{{.mainTargetGroupPort}}"
  {{.mainTargetGroupClusterRole}}_lbint_idle_timeout = "{{.idleTimeout}}"
  {{.mainTargetGroupClusterRole}}_lbint_sg_ids       = [
  {{- range $i, $sg := .securityGroups}}
    "{{$sg}}",
  {{- end}}
  ]
  {{.mainTargetGroupClusterRole}}_lbint_tags = {
    Name          = "${module.{{.serviceName}}_lbint_name.name}"
    Service       = "{{.serviceName}}"
    ProductDomain = "{{.productDomain}}"
    Environment   = "production"
    Description   = "{{.description}}"
  }
  {{.serviceName}}_lbint_healthcheck = {
    interval            = "{{.mainTargetGroupHealthCheckInterval}}"
    path                = "{{.mainTargetGroupHealthCheckPath}}"
    port                = "traffic-port"
    protocol            = "{{.mainTargetGroupHealthCheckProtocol}}"
    timeout             = "{{.mainTargetGroupHealthCheckTimeout}}"
    healthy_threshold   = "{{.mainTargetGroupHealthCheckHealthyThreshold}}"
    unhealthy_threshold = "{{.mainTargetGroupHealthCheckUnhealthyThreshold}}"
    matcher             = "{{.mainTargetGroupHealthCheckMatcher}}"
  }
}            

module "{{.serviceName}}_lbint_name" {
  source        = "git::https://github.com/traveloka/terraform-aws-resource-naming.git?ref=v0.8.0"
  name_prefix   = "${local.{{.serviceName}}_lbint_prefix}"
  resource_type = "lb"
}

module "{{.serviceName}}_tgint_name" {
  source        = "git::https://github.com/traveloka/terraform-aws-resource-naming.git?ref=v0.8.0"
  name_prefix   = "${local.{{.serviceName}}_tgint_prefix}"
  resource_type = "lb_target_group"
}

module "{{.serviceName}}_lbint" {
  source = "git::ssh://git@git.traveloka.com/source/terraform-aws-lb.git?ref=v0.1.11"

  vpc_id            = "${local.vpc_id}"
  domain_name       = "${local.domain_name}"
  service_name      = "${local.service_name}"
  main_service_port = "${local.{{.mainTargetGroupClusterRole}}_service_port}"
  idle_timeout      = "${local.{{.mainTargetGroupClusterRole}}_lbint_idle_timeout}"

  lb_security_group_ids = "${local.{{.mainTargetGroupClusterRole}}_lbint_sg_ids}"

  name     = "${module.{{.serviceName}}_lbint_name.name}"
  internal = "{{.isInternal}}"
  lb_cert_arn        = "${local.{{.serviceName}}_lbint_cert_arn}"
  access_logs_bucket = "${local.{{.serviceName}}_lbint_log_bucket}"

  lb_tags = "${local.{{.mainTargetGroupClusterRole}}_lbint_tags}"

  # LB target group parameters
  main_target_group_name                 = "${module.{{.serviceName}}_lbint_name.name}}"
  main_cluster_instance_ids              = []
  main_cluster_instance_count            = "0"
  maint_target_group_stickiness_duration = "${local.{{.serviceName}}_tgint_stickiness_duration}"
  main_target_group_healthcheck          = "${local.{{.serviceName}}_lbint_healthcheck}"
  main_target_group_tags                 = "${local.{{.serviceName}}_lbint_tags}"

  additional_cluster_instance_ids   = []
  additional_cluster_instance_count = "0"
}
`

// tfAppLoadBalancerCmd represents the tfAppLoadBalancer command
var tfAppLoadBalancerCmd = &cobra.Command{
	Use:   "tf-app-load-balancer",
	Short: "command to generate application load balancer terraform config",
	Run:   tfAppLoadBalancer,
}

func tfAppLoadBalancer(cmd *cobra.Command, args []string) {
	resources = extractResources(sourcePath)
	for i := 0; i < len(resources.Resources.Alb); i++ {
		tmpAlb := resources.Resources.Alb[i]
		var groupList []string
		for j := 0; j < len(tmpAlb.SecurityGroups); j++ {
			groupSplit := strings.Split(tmpAlb.SecurityGroups[j], "-")
			if groupSplit[0] == tmpAlb.ServiceName {
				tmp := "${aws_security_group." + tmpAlb.SecurityGroups[j] + ".id}"
				groupList = append(groupList, tmp)
			} else {
				tmp := awsGetGroupId(tmpAlb.SecurityGroups[j], getVPCId(vpcName))
				groupList = append(groupList, tmp)
			}
		}

		varmap := map[string]interface{}{
			"productDomain":                                resources.ProductDomain,
			"serviceName":                                  tmpAlb.ServiceName,
			"isInternal":                                   tmpAlb.Internal,
			"description":                                  tmpAlb.Description,
			"idleTimeout":                                  tmpAlb.IdleTimeout,
			"enableDeletionProtection":                     tmpAlb.EnableDeletionProtection,
			"mainTargetGroupClusterRole":                   tmpAlb.MainTargetGroupClusterRole,
			"mainTargetGroupDescription":                   tmpAlb.MainTargetGroupDescription,
			"mainTargetGroupProtocol":                      tmpAlb.MainTargetGroupProtocol,
			"mainTargetGroupPort":                          tmpAlb.MainTargetGroupPort,
			"mainTargetGroupStickinessCookieDuration":      tmpAlb.MainTargetGroupStickinessCookieDuration,
			"mainTargetGroupHealthCheckProtocol":           tmpAlb.MainTargetGroupHealthCheckProtocol,
			"mainTargetGroupHealthCheckPath":               tmpAlb.MainTargetGroupHealthCheckPath,
			"mainTargetGroupHealthCheckHealthyThreshold":   tmpAlb.MainTargetGroupHealthCheckHealthyThreshold,
			"mainTargetGroupHealthCheckUnhealthyThreshold": tmpAlb.MainTargetGroupHealthCheckUnhealthyThreshold,
			"mainTargetGroupHealthCheckTimeout":            tmpAlb.MainTargetGroupHealthCheckTimeout,
			"mainTargetGroupHealthCheckInterval":           tmpAlb.MainTargetGroupHealthCheckInterval,
			"mainTargetGroupHealthCheckMatcher":            tmpAlb.MainTargetGroupHealthCheckMatcher,
			"securityGroups":                               groupList,
			"vpcId":                                        getVPCId(vpcName),
		}

		if tmpAlb.Internal {
			tmpl, err := template.New("alb_resource").Parse(intAppLoadBalancerResource)
			if err != nil {
				log.Fatal(err)
			}
			tmpl.Execute(os.Stdout, varmap)
		}
	}
}

func init() {
	codegenCmd.AddCommand(tfAppLoadBalancerCmd)

	tfAppLoadBalancerCmd.Flags().StringVarP(&sourcePath, "file", "i", "", "source file path")
	tfAppLoadBalancerCmd.MarkFlagRequired("file")
	tfAppLoadBalancerCmd.Flags().StringVarP(&vpcName, "vpc-name", "p", "", "vpc tag name")
	tfAppLoadBalancerCmd.MarkFlagRequired("vpc-name")
}
