# opscli
*opscli* is CLI based tools to work with cloud operations.
This tools designed to be used by Traveloka cloud infra operations team.

## How to Install
1. Clone the repo
2. Run `dep ensure` in root directory
3. Build by running `make`
4. Move `opscli` into your `PATH`
5. Create configuration file at `$HOME/.opscli.yml`

## Configuration
Below is example of `opscli` configuration, please do store
at `$HOME/opscli.yml`:
```yaml
jira:
  url: https://example.atlassian.net
  username: test
  password: testpassword

```

## Features
### JIRA
#### List Attachments
`opscli jira list-attachments` provides function to list attachments
in specific issue. This is useful to get the attachment ID and 
newest attachment to be downloaded.
#### Download Attachment
`opscli jira download-attachment` provides function to download attachment
in specific issue. Useful to get attachment that contains resources 
definition of specific request.

### Codegen
#### Security Group
`opscli codegen tf-security-group` provides function to generate
terraform config for security group. It require resource definition 
as the input.
#### Autoscaling Group
`opscli codegen tf-autoscaling-group` provides function to generate
terraform config for auto scaling group. It require resource definition 
as the input.